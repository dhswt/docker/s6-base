docker-image = registry.gitlab.com/dhswt/docker/s6-base

buster-image:
	docker build \
		--squash \
		-t $(docker-image):debian-buster \
		-f Dockerfile.Debian-Buster \
		.

buster-run: buster-image
	docker run \
		-ti \
		--rm \
		-p 2222:22 \
		-v $(PWD)/test/authorized_keys:/authorized_keys \
		-e ENABLE_SSHD=1 \
		-e ENABLE_CRON=1 \
		-e SFTP_USERS="foo:bar:1001" \
		$(docker-image):debian-buster

buster-enter: buster-image
	docker run \
		-ti \
		--rm \
		-p 2222:22 \
		-v $(PWD)/test/authorized_keys:/authorized_keys \
		-e ENABLE_SSHD=1 \
		-e ENABLE_CRON=1 \
		--entrypoint bash \
		$(docker-image):debian-buster

show-images:
	docker images | grep "$(docker-image)"

# Remove dangling images
clean-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
		--filter "dangling=true" \
	| xargs docker rmi

# Remove all images
clear-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
	| xargs docker rmi
