#!/usr/bin/with-contenv bash
set -Eeo pipefail

ENABLE_SSHD=${ENABLE_SSHD:-0}

if [ "${ENABLE_SSHD}" != "1" ]; then
    echo "Disabling service: sshd"
    rm -rf /etc/services.d/sshd
    exit 0
fi

# prepare run dir
mkdir /run/sshd
chmod 600 /run/sshd

# use volume dir or secret if possible
if [ -d /ssh_host_keys ]; then

    # generate keys in volume if writable
    if [ -w /ssh_host_keys ]; then
        if [ ! -f /ssh_host_keys/ssh_host_ed25519_key ]; then
            ssh-keygen -t ed25519 -f /ssh_host_keys/ssh_host_ed25519_key -N ''
        fi
        if [ ! -f /ssh_host_keys/ssh_host_rsa_key ]; then
            ssh-keygen -t rsa -b 4096 -f /ssh_host_keys/ssh_host_rsa_key -N ''
        fi
    fi

    echo "Copy ssh host keys from /ssh_host_keys/ssh_host_* to /etc/ssh/"
    cp /ssh_host_keys/ssh_host_* /etc/ssh/
    chmod 600 /etc/ssh/ssh_host_*
    chmod 644 /etc/ssh/ssh_host_*.pub
fi

# Generate unique ssh keys in-place if volume/secret did not provide any
if [ ! -f /etc/ssh/ssh_host_ed25519_key ]; then
    ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
fi
if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
    ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ''
fi

# generate ssh key if missing
#if [[ ! -e /etc/ssh/ssh_host_rsa_key ]]; then
#    echo "No SSH host key available. Generating one..."
#    export LC_ALL=C
#    export DEBIAN_FRONTEND=noninteractive
#    dpkg-reconfigure openssh-server
#fi

# appending public keys for access (legacy, use $HOME/.ssh/keys instead)
if [[ -d /authorized_keys ]]; then
    echo "Appending ssh public keys from /authorized_keys/* to /root/.ssh/authorized_keys"
    cat /authorized_keys/* >> /root/.ssh/authorized_keys
fi

# Paths
userConfPath="/etc/sftp/users.conf"
userConfFinalPath="/var/run/sftp/users.conf"
reArgSkip='^([[:blank:]]*#.*|[[:blank:]]*)$' # comment or empty line

# Create users only on first run
if [ ! -f "$userConfFinalPath" ]; then
    setup-user-pubkey "www-data"
    setup-user-pubkey "root"

    mkdir -p "$(dirname $userConfFinalPath)"

    if [ -f "$userConfPath" ]; then
        # Append mounted config to final config
        grep -v -E "$reArgSkip" < "$userConfPath" > "$userConfFinalPath"
    fi

    if [ -n "$SFTP_USERS" ]; then
        # Append users from environment variable to final config
        IFS=" " read -r -a usersFromEnv <<< "$SFTP_USERS"
        for user in "${usersFromEnv[@]}"; do
            echo "$user" >> "$userConfFinalPath"
        done
    fi

    # Check that we have users in config
    if [ -f "$userConfFinalPath" ] && [ "$(wc -l < "$userConfFinalPath")" -gt 0 ]; then
        # Import users from final conf file
        while IFS= read -r user || [[ -n "$user" ]]; do
            create-sftp-user "$user"
        done < "$userConfFinalPath"
    fi
fi