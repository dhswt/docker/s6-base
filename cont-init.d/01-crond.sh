#!/usr/bin/with-contenv bash
ENABLE_CRON=${ENABLE_CRON:-0}

if [ "${ENABLE_CRON}" != "1" ]; then
    echo "Disabling service: cron"
    rm -rf /etc/services.d/cron
    exit 0
fi

# Touch cron files to fix 'NUMBER OF HARD LINKS > 1' issue. See  https://github.com/phusion/baseimage-docker/issues/198
touch -c /var/spool/cron/crontabs/*
touch -c /etc/crontab
touch -c /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/*
