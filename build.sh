#!/bin/bash
# enable -e to fail-fast on first error
set -e

### vars
DEBIAN_FRONTEND=noninteractive


### update system
apt-get update
apt-get upgrade -y


### download and extract s6 overlay
tar xfz /tmp/s6-overlay-amd64.tar.gz


### cron
# install packages (no recommends to avoid cron dependencies on exim and mysql)
apt -y install --no-install-recommends \
    cron

# Fix cron issues in 0.9.19, see also #345: https://github.com/phusion/baseimage-docker/issues/345
sed -i 's/^\s*session\s\+required\s\+pam_loginuid.so/# &/' /etc/pam.d/cron
chmod 600 /etc/crontab

# Remove all existing cron entries, they should never be executed
rm -f \
    /etc/cron.d/* \
    /etc/cron.hourly/* \
    /etc/cron.daily/* \
    /etc/cron.weekly/* \
    /etc/cron.monthly/*


### sshd
# install packages
apt -y install --no-install-recommends \
    openssh-server

# remove existing ssh host keys
rm -f /etc/ssh/ssh_host_*

# make scripts executable
chmod +x /usr/local/bin/*

# prepare root .ssh dir
mkdir -p /root/.ssh
chmod 700 /root/.ssh
chown root:root /root/.ssh
touch /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

### cleanup apt files and and temporary data
apt-get clean
rm -rf \
    /var/lib/apt/lists/* \
    /tmp/* \
    /var/tmp/*
